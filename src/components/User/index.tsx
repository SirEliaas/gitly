import React from 'react';
import { Text, View } from 'react-native';

export interface UserProps {
    login: string;
    avatar_url: string;
    bio: string;
    location: string;
    created_at: string;
}

const User: React.FC<UserProps> = ({ login, avatar_url, bio, location }) => {
    return (
        <View>
            <Text>{ bio }</Text>
        </View>
    );
};

export default User;